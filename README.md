# Project Title
Field of study: Advanced Applied Electronics <br >
Course: Real Time Operating Systems <br >
Laboratory list no.3.  <br >
author: Bartosz Michalak <br>

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Tasks description](#tasks)

## About <a name = "about"></a>

This project is created to implement multithread applications for RTOS
laboratory classes based on Xenomai. Apps are written in C using Xenomai API. 

## Getting Started <a name = "getting_started"></a>

Short introduction just to run code related to tasks. They are divided into two directories, every one has a similar structure. There is Makefile prepared to build task-related app. 

### Prerequisites

Apps were written and tested on prepared Virtual Machine by the lecturer based on ubuntu with xenomai installed and configured.

### Building and Running apps <a name = "usage"></a>

Example of usage for one of the tasks.


```sh
cd task3
make
./task3
```

## Tasks descriptions <a name = "tasks"></a>

**Task1 and 2** <br > 

As it was mentioned in the task description, code snippet was edited to compile and run a periodic task.
There is a while loop that periodically increments counter and prints its value on STDOUT. The task is executed until the 'Ctrl-c' (SIGINT) signal is emitted. 
```c
void demo(void *arg)
{
  RT_TASK *curtask;
  RT_TASK_INFO curtaskinfo;

  curtask = rt_task_self();
  rt_task_inquire(curtask, &curtaskinfo);
  printf("RTOS task1_2 - Hello! \n");
  volatile int counter = 0;
  //Print the info
  printf("Taskname:%s \n", curtaskinfo.name);

  //Make the task periodic with a specified loop period
  rt_task_set_periodic(NULL, TM_NOW, 1000000000);

  //Start the task loop
  while(1){
    printf("Loop number:%d \n", counter);
    counter++;
    rt_task_wait_period(NULL);
  }
}


```

**Task 3** 

The code is configured to show the behavior of a data race and to show the solution for that issue. If you want to see how to handle race condition you need to uncomment line 9.  

`#define WITH_MUTEX`

The goal of this app is to count to 10000, each task has to increment counter 5000 times. 
With this line commented there will be undefined behavior of the app because two tasks are using the same variable at the same time without any control.
You can see that mutex locks task while incrementing and printing counter, so only one task can use it at that moment, at the output You will see that counter has value equal 10000 - as desired

```c
void race_example(void *arg)
{
  RT_TASK *curtask;
  RT_TASK_INFO curtaskinfo;
  curtask = rt_task_self();
  rt_task_inquire(curtask, &curtaskinfo);
 
  int *cnt = (int*)arg;
  for(int i=0; i<5000; i++){
        printf("Taskname:%s \n", curtaskinfo.name);
#ifdef WITH_MUTEX
	rt_mutex_acquire(&mutex, TM_INFINITE);
#endif
	int copy = *cnt+1;
	rt_task_sleep(1);
	*cnt = copy;
	printf("Counter:%d \n", *cnt);
#ifdef WITH_MUTEX
	rt_mutex_release(&mutex);
#endif
	
  }
}
```
