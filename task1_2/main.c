#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <alchemy/task.h>
#include <alchemy/timer.h>


RT_TASK demo_task;

void demo(void *arg)
{
  RT_TASK *curtask;
  RT_TASK_INFO curtaskinfo;

  curtask = rt_task_self();
  rt_task_inquire(curtask, &curtaskinfo);
  printf("RTOS task1_2 - Hello! \n");
  volatile int counter = 0;
  //Print the info
  printf("Taskname:%s \n", curtaskinfo.name);

  //Make the task periodic with a specified loop period
  rt_task_set_periodic(NULL, TM_NOW, 1000000000);

  //Start the task loop
  while(1){
    printf("Loop number:%d \n", counter);
    counter++;
    rt_task_wait_period(NULL);
  }
}

int main(int argc, char **argv)
{
  char str[20];

  //Lock the memory to avoid memory swapping for this program
  mlockall(MCL_CURRENT | MCL_FUTURE);
    
  printf("Start task\n");

  sprintf(str, "demo task");

  rt_task_create(&demo_task, str, 0, 50, 0);

  rt_task_start(&demo_task, demo, NULL);

  //Wait for Ctrl-C
  pause();

  return 0;
}
