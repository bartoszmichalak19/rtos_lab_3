#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/mutex.h>

//#define WITH_MUTEX

RT_TASK data_race;
RT_TASK data_race2;
RT_MUTEX mutex;

void race_example(void *arg)
{
  RT_TASK *curtask;
  RT_TASK_INFO curtaskinfo;
  curtask = rt_task_self();
  rt_task_inquire(curtask, &curtaskinfo);
 
  int *cnt = (int*)arg;
  for(int i=0; i<5000; i++){
        printf("Taskname:%s \n", curtaskinfo.name);
#ifdef WITH_MUTEX
	rt_mutex_acquire(&mutex, TM_INFINITE);
#endif
	int copy = *cnt+1;
	rt_task_sleep(1);
	*cnt = copy;
	printf("Counter:%d \n", *cnt);
#ifdef WITH_MUTEX
	rt_mutex_release(&mutex);
#endif
	
  }
}

int main(int argc, char **argv)
{
  int data_race_counter = 0;
    mlockall(MCL_CURRENT | MCL_FUTURE);
    
  printf("Start task\n");

  rt_task_create(&data_race, "task1", 0, 50, T_JOINABLE);
  rt_task_create(&data_race2, "task2", 0, 50, T_JOINABLE);
  rt_mutex_create(&mutex,"");

  rt_task_start(&data_race, race_example , &data_race_counter);
  rt_task_start(&data_race2, race_example, &data_race_counter);

  rt_task_join(&data_race);
  rt_task_join(&data_race2);
  
  return 0;
}
